import { Meta } from "@angular/platform-browser";

export class Products {
    image?: string ;
    name?: string ;
    price?: number ;
}

export class Res <T> {
    data? : T
    meta? : Meta
}

export class Response <Res> {
    errors? : string
    message? : string
    status? : number
    response? : Res
}