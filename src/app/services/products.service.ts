import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Products, Response, Res } from '../models/products';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }
   
  url='http://adbuy-backend.kion.tech/api/products';
  baseUrl= 'http://adbuy-backend.kion.tech/';
  getProduct(): Observable<Response<Res<Products[]>>> {
    return this.http.get<Response<Res<Products[]>>>(this.url).pipe(
      map(res => {
        res.response.data.forEach(product => {
          product.image = `${this.baseUrl}${product.image}`;
         
        })
        return res;
      })
    ); 
  }
  
}
